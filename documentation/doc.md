Ce fichier décrit le fonctionnement global de trois scripts python
permettant la consultation "automatique" des copies par les étudiants
grâce à un envoi par mail.

# Fonctionnement général

### Avant l'examen
Vous générez puis imprimez des QR codes autocollants contenant 
les adresses mails des étudiants composant.


### Pendant l'examen
Vous collez les QR codes sur les copies des étudiants.


### Après l'examen
Vous scannez le paquet de copies en une fois,
la copie de chaque étudiant lui est automatiquement envoyée par mail.


# Dépendances

Les dépendances sont indiquées dans le fichier [`requirements.txt`](./requirements.txt).

Si vous avez déjà `pip`, `wheel` et `setuptools`, 
vous pouvez installer les modules demandés en lançant la commande suivante dans un terminal.

`python3 -m pip install <nom_du_module>`

Si le module est déjà installé, mais dans une version antérieure à celle requise,
vous pouvez le mettre à jour avec la commande suivante :

`python3 -m pip install --upgrade <nom_du_module>`

Sinon, vous pouvez consulter cette page d'aide à l'installation de modules : 

[`https://packaging.python.org/tutorials/installing-packages/#id11`](https://packaging.python.org/tutorials/installing-packages/#id11).

**Attention**, sur Mac, pour l'installation du package PyMuPDF, 
mieux vaut suivre les instructions données sur la page de GitHub de ce module :

[`https://github.com/pymupdf/PyMuPDF/blob/1.18.14/README.md#osx`](https://github.com/pymupdf/PyMuPDF/blob/1.18.14/README.md#osx).


# Scripts

3 scripts sont fournis dans le dossier [`sources`](../sources), 
et leur utilisation est décrite dans la section suivante. 

-   [`qr_generator.py`](../sources/qr_generator.py),
		ce script permet la génération d'un fichier PDF
   	contenant les QR codes des étudiants à partir d'une liste au format CSV.
   	Le format de ce PDF permet de l'imprimer sur du papier à étiquettes.
   	
-   [`splitter.py`](../sources/splitter.py),
    ce script permet de découper automatiquement un PDF
    contenant plusieurs copies d'étudiants en un PDF par étudiant et permet
    l'extraction des informations contenues dans les QR codes.
    
-   [`mailer.py`](../sources/mailer.py), 
    ce script permet l'envoi automatique de mails aux étudiants,
    leur copie se trouvant en pièce jointe du mail. 
    Ce script nécessite une authentification
    via CAS pour envoyer les mails avec votre adresse `@sorbonne-universite.fr`.


# Utilisation des scripts


## `qr_generator.py`

Usage : `./qr_generator.py [-i inputfile] [-o outputfile]  [-u ue] [-d date] [-s salle] [-q]`

Ce script prend en argument un fichier au format CSV (`inputfile`) qui liste les
étudiants composant dans une même salle. Chaque ligne concerne un étudiant et 
a le format `nom, prénom, numéro, mail`, où numéro peut-être un numéro d'étudiant
ou un numéro d'anonymat. Il produit un fichier PDF (`outputfile`) avec une étiquette
pour chaque étudiant listé contenant :

- un QR code encodant son adresse mail ;

- ses noms et prénoms ou bien son numéro pour une épreuve anonyme ;

- la date (`date`), la salle (`salle`) et le nom de l'UE (`ue`).

Lancé sans argument, ce script propose une interface dans le terminal pour
rentrer tous les paramètres nécéssaires à la création du PDF. Lors de la 
première execution, les paramètres définissant le format des planches 
d'étiquettes sont enregistrés dans un fichier nommé `param_etiquettes.json`.
Par la suite, si un fichier nommé `param_etiquettes.json` est détecté, 
l'interface propose d'utiliser les paramètres enregistrés dans ce fichier.

Si tous les arguments optionels sont donnés, et l'option `-q` activée,
le script s'execute sans interface, en utilisant les paramètres définis
dans un fichier nommé `param_etiquettes.json`.

Un exemple de script shell nommé [`script_multi_csv.sh`](../exemples/script_multi_csv.sh)
qui applique `qr_generator.py` à tous les fichiers CSV du dossier courant 
est proposé dans le dossier [`exemples`](../exemples)
dans le but de faciliter la génération des étiquettes
pour une grosse épreuve se déroulant dans de nombreuses salles.


## `splitter.py`

Usage : `./splitter.py [-o output_directory] [-s summary] [-c previous_summary] [-p precision] [-n nbp] PDF` 

Ce script prend en argument un PDF et le découpe en plusieurs PDF en
suivant le découpage induit par les QR codes. Chaque page du PDF original
doit contenir au plus un QR code, le PDF est alors découpé sur chaque page
contenant un QR code (i.e. le QR code est en première page du PDF découpé).

Si les copies ont un nombre `nbp` fixe de pages, ce nombre peut être renseigné
en utilisant l'option `-n` en ligne de commande afin d'accélérer le processus.
En effet dans ce cas seule 1 page sur `nbp`.
Dans le cas de scan recto-verso, `nbp` doit être pair.
Par exemple si chaque copie comporte 5 pages, chaque copie correspond à 3 feuilles
soit 6 pages même si la sixième est blanche.

Le script stocke les fichiers PDF ainsi obtenus dans un dossier qui se 
nomme par défaut `output`. Le nom de ce dossier peut être modifié en 
utilisant l'argument optionel `-o` en ligne de commande.

L'exécution du script produit un fichier qui se nomme par défaut `summary`.
Ce nom peut être modifié en utilisant l'argument optionel `-s` en ligne de
commande. Il s'agit d'un fichier CSV dont les lignes représentent les 
différentes copies extraites du PDF passé en argument. 
Chaque ligne a le format `email, PDF, start, end` où :

-   `email` est l'email de l'étudiant (extrait du QR code) ;

-   `PDF` est le chemin vers la copie de cet étudiant (au format PDF) ;

-   `start` est le numéro de page de début dans le PDF original ;

-   `end` est le numéro de page de fin dans le PDF original.

L'information contenue dans ce fichier sera utilisée pour envoyer
automatiquement les mails, mais vous sert aussi à vérifier que le 
découpage est correct.

Le niveau de précision avec lequel les pages sont traitées peut 
être ajusté en utilisant l'argument optionel `-p`.
Les valeurs possibles sont :
-   `b` pour bas (valeur par défaut) ;
-   `m` pour moyen ;
-   `h` pour haut.

Si le découpage n'est pas correct, il est possible de modifier le fichier `summary`.
Il faut alors **relancer le script `splitter.py`** en passant 
en argument ce fichier `summary` avec l'option `-s`. 
En tel cas, le script n'utilisera plus les QR codes contenus dans le PDF original 
pour faire le découpage mais les numéros de page indiqués dans le fichier `summary`.


## `mailer.py`

Usage : `./mailer.py [-c mail_config] [-s summary_file]`

Ce script utilise le fichier `summary` produit par `splitter.py` ainsi 
que les fichiers PDF générés par `splitter.py` pour envoyer automatiquement
un mail aux étudiants concernés. 

Par défaut, le script `mailer.py` va chercher un fichier nommé `summary` dans le dossier courant
pour avoir la liste des fichiers à envoyer et les destinataires correspondants.
Il est possible de changer le fichier `summary` utilisé grâce à l'option  `-s`.

Par défaut, le format de mail utilisé est défini dans fichier [`default_mail_config.json`](../sources/default_mail_config.json),
qui doit se trouver dans le dossier courant.
Il est possible de changer le fichier utilisé pour la configuration des mails grâce à l'option  `-c`.
Le format par défaut ne présente ni le nom de l'ue, ni même votre adresse mail en expéditeur.
Vous pouvez modifier le fichier `default_mail_config.json` à la main pour indiquer 
l'expéditeur, le sujet, le corps du message et une ou plusieurs adresses à mettre en copie,
mais le fichier doit garder sa structure à savoir :

`{`

`"exp":" <expediteur@messagerie.fr> ",`

`"subject":"<Sujet du message>",`

` "body":"<Corps du message>",`

` "cc": "<adresse en copie>, <autre adresse en copie>",`

`}`.

Notez que pour garder une trace des messages envoyés sur votre messagerie,
il faut indiquer votre adresse en CC.


Le script vous demandera de vous identifier auprès du serveur CAS de
Sorbonne Université afin d'établir la connexion avec votre messagerie
Sorbonne Université.


# Pour implémenter cette solution dans un autre établissement
Ces scripts ont été écrits en vue d'une utilisation par des membres de Sorbonne Université,
mais il est très facile de les modifier pour un autre établissement.
En effet, l'établissement n'intervient que lors de l'envoi des mails,
on utilise ici le serveur des mails de Sorbonne Université, 
mais on aurait pu utiliser un autre serveur (celui du laboratoire, ou bien gmail, outlook...).

Pour modifier le serveur utilisé, il suffit de changer les deux paramètres indiqués
dans la fonction `connect_smtp_server_SU` du fichier [`mailer.py`](../sources/mailer.py).
Actuellement, `"imaps.sorbonne-universite.fr"` indique le serveur utilisé
et `465` le port utilisé pour l'envoi.
Pour connaître le serveur et le port de la messagerie que vous voulez utiliser,
cherchez comment configurer Thunderbird, ou votre application de messagerie sur mobile.


#!/usr/bin/python3

from pyzbar.pyzbar import decode
import fitz
import os
import sys
import getopt
import numpy as np
import cv2
from scipy.interpolate import splprep, splev
import csv

def treat_image(img):
    mask = cv2.inRange(img,(0,0,0),(200,200,200))
    thresholded = cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
    inverted = 255-thresholded
    return inverted

def marks_from_qrcodes(filename, one_in, prec=1, try_hard=False):
    """takes as input a filename and returns an association list binding integers
    (coding for starting page) to email adresses and student number decoded
    from qrcode

    """
    file_handle = fitz.open(filename)
    # list storing result
    marks=[]
    if one_in == None:
        one_in = 1
    print("precision level: {}, {}".format(prec, try_hard))
    for i in range(len(file_handle)):
        if i % one_in != 0: continue
        print("reading page {}".format(i+1), end='')
        page=file_handle[i]     # read i-th page
        pix = page.get_pixmap(matrix=fitz.Matrix(prec, prec),alpha = False)  # convert to pixmap
        imgData = pix.getImageData("png")
        nparr = np.frombuffer(imgData, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        # cv2.imwrite(str(i)+".png", img)
        result=decode(img)      # trying to find a qrcode in the page
        if len(result) != 0:
            marks.append((i, result[0].data.decode("utf-8")))
            print(u'\t\u2713')
        else :
            if try_hard:
                img = treat_image(img)
                img = cv2.bilateralFilter(img,9,75,75)
                # cv2.imwrite(str(i)+".treated.png", img)
                result=decode(img)
                if (len(result) != 0):  # found one
                    marks.append((i, result[0].data.decode("utf-8")))
                    print(u'\t\u2713')
                else:
                    print()
            else:
                print()
    return marks

def marks_from_cutter(cutter) :
    marks = []
    with open(cutter, newline='') as cuttercsv:
        csvreader=csv.reader(cuttercsv, delimiter=',')
        for row in csvreader:
            marks.append((int(row[2]), row[0]))
    marks = [(start-1, x) for (start, x) in marks]
    return marks

def extract(file_handle, start, end, output):
    doc = fitz.open()
    doc.insert_pdf(file_handle, from_page = start, to_page = end)
    doc.save(output)

def cut_pdf(filename, marks, output):
    """cut pdf filename according to the marks association list which associates
    page number to email adresses. Produces for each binding in marks a .pdf
    and a .txt file in output. These two files are indexed by the same integer,
    the .txt file contains the email address.

    """
    inputpdf = fitz.open(filename)
    inputpdfsize  = len(inputpdf)

    lenmarks = len(marks)
    enrichedmarks=[]

    summary = []

    for i in range(len(marks)):
        if i == lenmarks-1:
            enrichedmarks.append((marks[i][0], inputpdfsize-1, marks[i][1]))
        else:
            enrichedmarks.append((marks[i][0], marks[i+1][0]-1, marks[i][1]))

    cpt=0
    for (start, end, mail) in enrichedmarks:
        filename = output + "/" + ("copie_%s.pdf" % cpt)
        extract(inputpdf, start, end, filename)
        cpt= cpt+1
        summary.append([mail, filename, str(start), str(end)])

    return summary

def write_summary(tab, summaryfile):
    tab = [(mail, filename, str(int(start)+1), str(int(end)+1)) for (mail, filename, start, end) in tab]
    with open(summaryfile, 'w', newline='') as csvfile:
        csvwriter= csv.writer(csvfile, delimiter=',')
        for row in tab:
            csvwriter.writerow(row)

def usage():
    print("Usage: ./splitter.py [-o output_directory] [-s output_summary] [-c previous_summary] pdf")

config = {"b": (1, False), "m": (3, False), "h": (6, True)}

def main(argv):
    output= "output"
    cutter= None
    summaryfile = None
    one_in = None
    prec=(config["b"])
    try:
        opts, args = getopt.getopt(argv, "ho:c:s:p:n:", ["help","output=","cutter=", "summary=","prec=", "nb_page="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            usage()
            sys.exit()
        elif opt in ['-o', '--output']:
            output=arg
        elif opt in ['-c', '--cutter']:
            cutter=arg
        elif opt in ['-s', '--summary']:
            summaryfile=arg
        elif opt in ['-p', '--prec']:
            prec=config[arg]
        elif opt in ['-n', '--nb_page']:
            one_in=int(arg)
    if len(args[0]) == 0:
        usage()
        sys.exit(2)
    else:
        if summaryfile==None:
            summaryfile=output + "/summary"
        if not os.path.exists(output):
            os.makedirs(output)
        inputfile=args[0]
        if cutter == None:
            marks=marks_from_qrcodes(inputfile, one_in, prec=prec[0], try_hard=prec[1])
        else:
            marks=marks_from_cutter(cutter)
        summary = cut_pdf(inputfile, marks, output)
        write_summary(summary, summaryfile)

main(sys.argv[1:])

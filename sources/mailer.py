#!/usr/bin/python3
#inspiré de https://github.com/MLasserre/openSanta/blob/master/santa.py

import email, smtplib, ssl
import getpass #pour rentrer son mot depasse
import getopt
import sys
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import glob
from typing import List
import json
import csv

def connect_smtp_server(user, password, smtp_server, port):
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.ehlo()
        server.starttls()
        server.login(user, password)
        print("Successfully connected to server")
        return server
    except smtplib.SMTPException as e:
        print("Failed to connect to server:",e) 



def connect_smtp_server_lip6(user, password):
    try:
        server = smtplib.SMTP_SSL("newmail.lip6.fr", 465)
        server.ehlo()
        server.login(user, password)
        print("Successfully connected to server")
        return server
    except smtplib.SMTPException as e:
        print("Failed to connect to server:",e) 


def connect_smtp_server_SU(user, password):
    try:
        server = smtplib.SMTP_SSL("imaps.sorbonne-universite.fr", 465)
        server.ehlo()
        server.login(user, password)
        print("Successfully connected to server")
        return server
    except smtplib.SMTPException as e:
        print("Failed to connect to server:",e) 


def send_msg(server, cc , fromaddr:str, toaddr: str, subject:str, body:str, attachement):
    try:
        # Create a multipart message and set headers
        message = MIMEMultipart()
        message["From"] = fromaddr
        message["To"] = toaddr
        if (cc != ""):
            message["Cc"] = cc
        message["Subject"] = subject

        # Add body to email
        message.attach(MIMEText(body, "plain"))

        # Open PDF file in binary mode
        with open(attachement, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email    
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        part.add_header(
            "Content-Disposition",
            'attachement',
            filename="copie.pdf"
        )

        # Add attachment to message and convert message to string
        message.attach(part)
        msg_complet = message.as_string()
        # print(msg_complet)

        rcpts = [toaddr]
        if (cc != ""):
            rcpts.append(cc)

        server.sendmail("sender@usl.fr", rcpts, msg_complet)

    except smtplib.SMTPException as e:
        print("Failed to send mail:",e) 


def disconnect_smtp_server(server):
    try:
        server.close()
        print("Successfuly disconnected from server")
    except smtplib.SMTPException as e:
        print("Failed to disconnect from server:",e)

def usage():
    print("Usage: ./mailer.py [-c mail_config] [-s summary_file]")

def main(argv):
    summaryfile= "./output/summary"
    config= "default_mail_config.json"
    try:
        opts, args = getopt.getopt(argv, "hc:s:", ["help", "config=", "summary="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            usage()
            sys.exit()
        elif opt in ['-c', '--config']:
            config=arg
        elif opt in ['-s', '--summary']:
            summaryfile=arg
    print("... Initiating connection to SU SMTP server")
    user_email = input('Enter your CAS login: ')
    user_password = getpass.getpass("Enter your CAS password: ")
    print("... Please wait")
    server = connect_smtp_server_SU(user_email, user_password)
    if (server == None):
        print("... Connection failed")
        sys.exit(2)
    else:
        print("... Connection established")
        students = []
        with open(summaryfile, newline='') as summarycsv:
            csvreader=csv.reader(summarycsv, delimiter=',')
            students = [(row[0], row[1]) for row in csvreader]

        with open(config) as f:
            datas = json.load(f)

        print("A mail will be sent to each of the following adresses : ")
        for s in students:
            print("  " + s[0])
        print("From adress : {}".format(datas["exp"]))
        if datas["cc"]!="":
        	print("With copy to : {}".format(datas["cc"]))
        print("With subject : {}".format(datas["subject"]))
        print("With body :\n==========\n{}\n==========".format(datas["body"]))
        rep = input("Please confirm (y/N) ")
        if (rep=='y') :
            for s in students:
                send_msg(server, datas["cc"], datas["exp"],s[0],datas["subject"],datas["body"],s[1])
                print('  Mail successfully sent to {}'.format(s[0]))
            print("All mails have been sent, Bye")
        else:
            print('Giving up, Bye')
        disconnect_smtp_server(server)



main(sys.argv[1:])

#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import List, Tuple, Dict

import argparse
import os
import shutil

import json
import csv

from qrcodegen import QrCode, QrSegment

import fitz #=PyMuPDF


#type étudiant : nom, prénom, numéro, mail
Etudiant= Tuple[str,str,int,str]

#VARIABLES GLOBALES
fichier_param_gen="param_etiquettes.json"
dossier_temp='qrcodes_temp'



######################################
# FONCTIONS AUXILIAIRES 1/2
######################################
def adapted_fontsize(fs_init :int, ft_name :str, txt : str, case: fitz.Rect) -> int:
	""" envoie la taille de police maximale permettant de faire tenir le texte txt dans le rectangle case
	avec la police définie dans format, parmi les tailles inférieures à celle par défaut dans format."""
	fs:int=fs_init
	if fitz.version[0] >= '1.18.14' :
		while fitz.getTextLength(txt, fontsize=fs, fontname=ft_name) > case.width or fs>case.height:
			fs=fs-1
	else:
		while fitz.getTextlength(txt, fontsize=fs, fontname=ft_name) > case.width or fs>case.height:
			fs=fs-1
	return fs


	
######################################
# CLASSE POUR LE FORMAT
######################################
class myFormat:
	"""Une classe pour les paramètres de format des planches d'étiquettes"""

	larg_feuille_cm =21.0 
	haut_feuille_cm =29.7	
	larg_feuille=595
	haut_feuille=842
	
	nbc=-1
	nbl=-1
	nb_etiq=-1
  	
	
	top_margin_cm=-1
	bottom_margin_cm=-1
	left_margin_cm=-1
	right_margin_cm=-1
	
	top_margin=-1
	bottom_margin=-1
	left_margin=-1
	right_margin=-1
		
	larg_etiq_cm=-1
	haut_etiq_cm=-1
	
	larg_etiq=-1
	haut_etiq=-1

	niveau_correction=QrCode.Ecc.MEDIUM
	niveau_correction_str="moyen"
	avec_noms= False
	avec_noms_str="num"
	
	securite= 3

	police= "helv"
	fontsize_nom = 12
	fontsize_num = 26
	fontsize_details=10
	
	ue=""
	date=""
	salle=""
		
	#-----------------------------------
	def __init__(self,nbc=3,nbl=8,top_margin_cm=1.3,bottom_margin_cm=1.3,left_margin_cm=0.7,right_margin_cm=0.7):
		"""cree un objet myFormat,
		Ne remplit que les paramètres généraux (ie ni ue, ni date, ni salle)
		Les paramètres niveau_correction, avec_noms, police, fontsize_* sont laissés à leur valeur par défaut.
		S'ils sont passés en paramètres, les nombres de ligne et colonnes, ainsi que les marges en cm sont mis à jour.
		Les marges et les tailles des étiquettes (ne cm et en pixels) sont calculées.
		"""
		
		self.nbl=nbl
		self.nbc=nbc
		self.nb_etiq : int = nbc*nbl #number of labels per sheet
		
		self.top_margin_cm= top_margin_cm
		self.bottom_margin_cm=bottom_margin_cm
		self.left_margin_cm=left_margin_cm
		self.right_margin_cm=right_margin_cm
		
		#convertir en pixel les paramètres
		(self.top_margin,self.bottom_margin)=convert_cm_to_px(top_margin_cm-0.3,bottom_margin_cm-0.3,self)
		(self.left_margin,self.right_margin)=convert_cm_to_px(left_margin_cm-0.3,right_margin_cm-0.3,self)
		#NB cause de l'imprimante on enlève 3mm aux marges indiquées		
		

		self.larg_etiq_cm : float= (self.larg_feuille_cm- left_margin_cm - right_margin_cm) / nbc #label width
		self.haut_etiq_cm : float= (self.haut_feuille_cm- top_margin_cm - bottom_margin_cm) / nbl #sheet height
			
		self.larg_etiq= (self.larg_feuille- self.left_margin - self.right_margin) / nbc  - 2*self.securite #label width
		self.haut_etiq= (self.haut_feuille- self.top_margin - self.bottom_margin) / nbl  - 2*self.securite #sheet height
		
		#on en profite pour régler la taille de la font pour les détails une fois pour toutes
		fs=self.fontsize_details
		if fitz.version[0] >= '1.18.14' : 
			while max(fitz.getTextLength("  "+self.ue, fontsize=fs, fontname=self.police),\
			fitz.getTextLength("  "	+self.ue, fontsize=fs, fontname=self.police),\
			fitz.getTextLength("  "+self.salle, fontsize=fs, fontname=self.police) )> self.larg_etiq/2-self.securite\
			 or (4*fs)>(self.haut_etiq/2-self.securite):
				fs=fs-1
		else :
			while max(fitz.getTextlength("  "+self.ue, fontsize=fs, fontname=self.police),\
				fitz.getTextlength("  "	+self.ue, fontsize=fs, fontname=self.police),\
				fitz.getTextlength("  "+self.salle, fontsize=fs, fontname=self.police) )> self.larg_etiq/2-self.securite\
				 or (4*fs)>(self.haut_etiq/2-self.securite):
					fs=fs-1

		self.fontsize_details=fs
		#print("***fontsize_details",self.fontsize_details,"et haut_etiq/2-securite=",self.haut_etiq/2-self.securite) 
			
	#-----------------------------------
	def affiche_format_fr(self):
		""" affiche le format de chaque étiquette"""
		print("Sur chaque feuille il y a", self.nb_etiq, "étiquettes de dimensions", format(self.larg_etiq_cm, '.1f'),"cm par", format(self.haut_etiq_cm, '.1f') ,"cm environ")
	
	#-----------------------------------
	def resume_param_gen_fr(self):
		"""Résume les paramètres généraux enregistrés dans l'objet.
			Ce qui est affiché correspond aux infos extraites du fichier json qui enregistre les paramètres généraux
		"""
		print(" | Format : A4")
		print(" | Orientation : portrait")
		print(" | Nombre de lignes :",str(self.nbl))
		print(" | Nombre de colonnes :",str(self.nbc))
		print(" | Marge en haut (en cm) :",str(self.top_margin_cm))
		print(" | Marge en bas (en cm) :",str(self.bottom_margin_cm))
		print(" | Marge à gauche (en cm) :",str(self.left_margin_cm))
		print(" | Marge à droite (en cm) :",str(self.right_margin_cm))
		print(" | Niveau de correction des erreurs des qr-codes :",self.niveau_correction_str)
		print(" | Affichage des noms prénoms ou des numéros :",self.avec_noms_str)
	
	
	#-----------------------------------
	def exporte_param_json(self, fichier_json:str):
		""" crée un fichier nommé fichier_json avec les paramètres généraux (contenus dans cet objet) encodés au format json"""
		param=dict()
		
		param["larg_feuille_cm"]=self.larg_feuille_cm
		param["haut_feuille_cm"]=self.haut_feuille_cm
		param["larg_feuille"]=self.larg_feuille
		param["haut_feuille"]=self.haut_feuille
		
		param["nbc"]=self.nbc
		param["nbl"]=self.nbl
		
		param["top_margin_cm"]=self.top_margin_cm
		param["bottom_margin_cm"]=self.bottom_margin_cm
		param["left_margin_cm"]=self.left_margin_cm
		param["right_margin_cm"]=self.right_margin_cm
		
		param["niveau_correction"]=self.niveau_correction_str
		param["avec_noms"]=self.avec_noms
		
		fichier=open (fichier_json,'w')
		json.dump(param,fichier)
		fichier.close()
    
    
    #fin de la classe		MyFormat



######################################		
def lit_param_gen_json(fichier_json:str)-> myFormat:
	""" Cree et retourne un objet myFormat à partir des paramètres généraux stockés dans le fichier fichier_json
	Si ce fichier n'existe pas, une exeption est levée
	Si ce fichier n'est pas valide, une autre exception esr levée
	"""
	if os.path.isfile(fichier_json):
		fichier=open(fichier_json,'r')
		"""
		s=fichier.read()[:-1]
		fichier.close()
		#on a enlevé le dernier caractère qui est un \n qui fait échouer json.load
		"""
		try:
			param=json.load(fichier)
			#param=json.loads(s)
		except:
			raise Exception("--- Fichier paramètres invalide")
		#for k in param:
		#	print(k,"->",param[k])
		mon_format=myFormat(param["nbc"],param["nbl"],param["top_margin_cm"],param["bottom_margin_cm"],param["left_margin_cm"],param["right_margin_cm"])
		mon_format.niveau_correction_str=param["niveau_correction"]
		if param["niveau_correction"]=="bas":
			mon_format.niveau_correction=QrCode.Ecc.LOW
		elif param["niveau_correction"]=="moyen":
			mon_format.niveau_correction=QrCode.Ecc.MEDIUM
		elif param["niveau_correction"]=="haut":
			mon_format.niveau_correction=QrCode.Ecc.HIGH
		else:
			raise Exception("--- fichier paramètres invalide\n--- plus précisément le paramètre niveau_correction est invalide\n--- seul \"bas\",\" moyen\" et \"haut\" sont acceptés")
		mon_format.avec_noms=param["avec_noms"]
		if mon_format.avec_noms:
			mon_format.avec_noms_str="nom"
		else:
			"num"
		return mon_format
	else:
		raise Exception("--- Pas de fichier paramètres détécté")



######################################
# FONCTIONS AUXILIAIRES 2/2
######################################
def convert_cm_to_px(x:float, y:float, f:myFormat)->(int,int):
	return( int((x*f.larg_feuille)/f.larg_feuille_cm),int((y*f.haut_feuille)/f.haut_feuille_cm) )
	
def convert_px_to_cm(x:int, y:int, f:myFormat)->(int,int):
	return( (x*f.larg_feuille_cm)/f.larg_feuille, (y*f.haut_feuille_cm)/f.haut_feuille)
	

######################################
#	FONCTION POUR SAISIE
######################################
def rentrer_param_gen()-> myFormat:
	"""Prend en compte des paramètres généraux par échange avec l'utilisateur via la console
	Exporte les paramètres obtenus dans un fichier json nommé fichier_param_gen (variable globale)
	Renvoie un objet myFormat initialisé avec ces paramètres généraux
	"""
	print("\nNous allons définir ensemble les paramètres généraux des étiquettes.")
	print("Ils seront enregistrés et automatiquement proposés la prochaine fois.")
	print("En cas d'erreur pour l'une des valeurs, deux options:")
	print(" -  tapez control+C et recommencez,")
	print(" -  finissez, ouvrez le fichier .json crée, modifiez le paramètre en question et recommencez.\n")
	
		
	print("\n------------ Format de la planche d'étiquettes")
	print("----------------------------------------------")
	#print("La feuille est supposée être au format A4, orientation portrait")
	print(" | Format : A4")
	print(" | Orientation : portrait")
	
	nbl=-1
	while nbl<= 0:
		try:
			nbl= int(input(" | Nombre de lignes : "))
			if nbl<=0:
				print("\n---Merci de rentrer un nombre de lignes strictement positif.")
		except ValueError:
			print("---La valeur saisie est incorecte.\n---Merci de rentrer un nombre de lignes entier, strictement positif, écrit en chiffre, sans points ni virgule.")
			
	nbc=-1
	while nbc<= 0:
		try:
			nbc= int(input(" | Nombre de colonnes : "))
			if nbc<=0:
				print("\n---Merci de rentrer un nombre de colonnes strictement positif.")
		except ValueError:
			print("---La valeur saisie est incorecte.\n---Merci de rentrer un nombre de colonnes entier, strictement positif, écrit en chiffre, sans points ni virgule.")
	
	dim_hauteur_ok=False
	while not dim_hauteur_ok:
		top_margin_cm=-1
		while top_margin_cm < 0:
			try:
				top_margin_cm=float(input(" | Marge en haut (en cm) : "))
			except ValueError:
				print("---La valeur saisie est incorecte.")
				print("---Merci de rentrer une valeur positive, écrite en chiffre, en utilisant si besoin un point et non une virgule.")

		bottom_margin_cm=-1
		while bottom_margin_cm < 0:
			try:
				bottom_margin_cm=float(input(" | Marge en bas (en cm) : "))
			except ValueError:
				print("---La valeur saisie est incorecte.")
				print("---Merci de rentrer une valeur positive, écrite en chiffre, en utilisant si besoin un point et non une virgule.")
				
		if top_margin_cm + bottom_margin_cm < myFormat.haut_feuille_cm:
			dim_hauteur_ok=True
		else:
			print("---Les valeurs saisies ne sont pas compatibles avec la hauteur de la feuille (par défaut de",myFormat.haut_feuille_cm,"cm).")
			print("---La somme de la marge du bas et de celle du haut doit être strictement inférieure à la hauteur de la feuille.")
	
	dim_largeur_ok=False
	while not dim_largeur_ok:
		left_margin_cm=-1
		while left_margin_cm < 0:
			try:
				left_margin_cm=float(input(" | Marge à gauche (en cm) : "))
			except ValueError:
				print("---La valeur saisie est incorecte.")
				print("---Merci de rentrer une valeur positive, écrite en chiffre, en utilisant si besoin un point et non une virgule.")

		right_margin_cm=-1
		while right_margin_cm < 0:
			try:
				right_margin_cm=float(input(" | Marge à droite (en cm) : "))
			except ValueError:
				print("---La valeur saisie est incorecte.")
				print("---Merci de rentrer une valeur positive, écrite en chiffre, en utilisant si besoin un point et non une virgule.")
				
		if left_margin_cm + right_margin_cm < myFormat.larg_feuille_cm:
			dim_largeur_ok=True
		else:
			print("---Les valeurs saisies ne sont pas compatibles avec la largeur de la feuille (par défaut de",myFormat.larg_feuille_cm,"cm).")
			print("---La somme des marges gauche et droite doit être strictement inférieure à la largeur de la feuille.")
	
	mon_format=myFormat(nbc,nbl,top_margin_cm,bottom_margin_cm,left_margin_cm,right_margin_cm)
	mon_format.affiche_format_fr()
	
	print("\n------------ Paramètres des étiquettes")
	print("--------------------------------------")
	
	niv_str=input(" | Niveau de correction des erreurs des qr-codes (b pour bas, m pour moyen ou h pour haut) : ")
	while niv_str not in {'b','m','h'}:#["bas","moyen","haut"]:
		#print("---Cette interface ne supporte que les réponses \"bas\", \"moyen\" et \"haut\"")
		print("---Cette interface ne supporte que les réponses \"b\", \"m\" et \"h\"")
		print("   données sans espace avant ni après ni majuscule")
		niv_str=input(" | Niveau de resistance aux erreurs des qr-codes (bas, moyen ou haut) : ")
	if niv_str=="b":
		mon_format.niveau_correction= QrCode.Ecc.LOW
		mon_format.niveau_correction_str="bas"
	elif niv_str=="m":
		mon_format.niveau_correction= QrCode.Ecc.MEDIUM
		mon_format.niveau_correction_str="moyen"
	else:
		mon_format.niveau_correction= QrCode.Ecc.HIGH
		mon_format.niveau_correction_str="haut"
		
	ac_nom_str=input(" | Affichage des noms prénoms ou des numéros (tapez nom ou num) : ")
	while ac_nom_str not in {"nom","num"}:
		print("---Cette interface ne supporte que les réponses \"nom\" et \"num\"")
		print("   données sans espace avant ni après ni majuscule")
		ac_nom_str=input(" | Affichage des noms prénoms ou des numéros (tapez nom ou num) : ")
	mon_format.avec_noms = (ac_nom_str=="nom")

	#créér le json
	mon_format.exporte_param_json(fichier_param_gen)
	print("\n\nCes paramètres ont été enregistrés dans le fichier",fichier_param_gen,"\n")
	
	return mon_format



######################################
def rentrer_fichier_entree_fr(args):
		print("\n\n\n------------  Fichier en entrée")
		print("-------------------------------")
		if args.inputfile==None :
			#fichier_entree_csv=input("Entrez le nom du fichier .csv à traiter (avec l'extension .csv) : ")
			fichier_entree_csv=input(" |  Nom du fichier .csv à traiter (avec l'extension .csv) : ")
		else:
			fichier_entree_csv=args.inputfile
			if os.path.isfile(args.inputfile):
				print(" | Nom du fichier .csv à traiter",fichier_entree_csv)
		while not os.path.isfile(fichier_entree_csv) :
			print("---Le fichier",fichier_entree_csv," est absent")
			fichier_entree_csv=input(" |  Nom du fichier .csv à traiter (avec l'extension .csv) : ")
		return fichier_entree_csv



######################################
def rentrer_param_part_fr(mon_format : myFormat,fichier_entree, arguments):
	"""prise en compte des paramètres particulier par échange avec l'utilisateur via la console
	Ces paramètres ne sont utilisés que pour le fichier.csv fichier_entree
	"""
	print("\n\n\n------------  Contenu des étiquettes")
	print("------------------------------------")
	
	print("Si vous ne les avez pas déjà passés en argument, nous allons définir ensemble la date, l'ue et la salle.")
	print("Ces paramètres sont spécifiques au fichier csv en cours (",fichier_entree,")")
	
	#ue
	if arguments.ue==None:
		mon_format.ue=input(" | Matière ou UE : ")
	else:
		mon_format.ue=arguments.ue
		print(" | Matière ou UE :",mon_format.ue)
		
	#date
	if arguments.date==None:
		mon_format.date=input(" | Date : ")
	else:
		mon_format.date=arguments.date
		print(" | Date :",mon_format.date)
	
	#salle
	if arguments.salle==None:
		mon_format.salle=input(" | Salle : ")
	else:
		mon_format.salle=arguments.salle
		print(" | Salle :",mon_format.salle)



		
######################################
def rentrer_fichier_sortie_fr(args):
	print("\n\n\n------------  Fichier de sortie")
	print("-------------------------------")
	if args.outputfile==None :
		fichier_sortie_pdf=input(" | Nom du fichier .pdf à produire (avec l'extension.pdf) : ")
		#fichier_sortie_pdf=input("Entrez le nom du fichier .pdf à produire (avec l'extension.pdf) : ")
	else:
		fichier_sortie_pdf=args.outputfile
		if fichier_sortie_pdf[-4:] == ".pdf" or fichier_sortie_pdf[-4:] == ".PDF" :
			print(" | Nom du fichier .pdf à produire :",fichier_sortie_pdf)
	while fichier_sortie_pdf[-4:] != ".pdf" and fichier_sortie_pdf[-4:] != ".PDF" :
		print("---Le nom de fichier \""+fichier_sortie_pdf+"\" ne finit pas par l'extension \".pdf\"")
		fichier_sortie_pdf=input(" | Nom du fichier .pdf à produire (avec l'extension.pdf) : ")
	if os.path.isfile(fichier_sortie_pdf):
		print("---Il existe déjà un fichier nommé \""+fichier_sortie_pdf+"\".")
		print("Si vous ne voulez pas qu'il soit écrasé, tapez control+C, puis recommencez")
		input("Pour continuer (et donc écraser le fichier), tapez entrée.  ")
	return fichier_sortie_pdf

	
######################################
def lit_csv(fichier_entree, verbeux:bool) -> List[Etudiant]:
	"""Cree et retourne une liste d'étudiant à partir du fichier csv nommé fichier_entree
	Ignore les lignes complètement vides du csv (ie 0 case, pas plusieurs cases vides)
	En cas de ligne ayant moins de 4 cases, la ligne est ignorée , avec message, 
	De plus dans ce cas un message de rappel apparaît une fois à la fin du fichier
	"""
	if verbeux:
		print("\n\n\n------------ Lecture du fichier .csv")
		print("------------------------------------")
	
	L_etu=[]
	rappel=False
	
	f= open(fichier_entree, newline='')
	reader = csv.reader(f)
	
	ligne=1
	for row in reader:
		if len(row) == 4:
			L_etu.append(row)
		else:
			#message d'alerte sauf si la ligne est complètement vide
			if row!=[]:
				#print("---la ligne",ligne,"semble incorrecte, elle n'a pas 4 champs mais",len(row),"\b, elle a donc été ignorée.")
				print("---ligne",ligne,"on lit", row)
				print("---cette ligne semble incorrecte, elle n'a pas 4 champs mais",len(row),"\b, elle a donc été ignorée.")
				rappel=True
		ligne =ligne+1
	
	f.close
	
	if rappel:
		print("---Pour rappel, le format de chaque ligne est ''nom, prénom, numéro, adresse mail'', sans virgule à la fin de la ligne.")
	
	if rappel or verbeux:
		print("En tout,", len(L_etu),"étudiants ont été reconnus.")
	
	return L_etu
	



######################################
# GENERATION DES QR-CODES
######################################
def print_qr(qrcode: QrCode) -> None:
	"""Prints the given QrCode object to the console."""
	border = 2
	for y in range(-border, qrcode.get_size() + border):
		for x in range(-border, qrcode.get_size() + border):
			print("\u2588 "[1 if qrcode.get_module(x,y) else 0] * 2, end="")
		print()
	print()


######################################
def genere_qr_code_svg(texte:str, fichier_sortie:str, niveau_correction):
	"""generates the qr-code encoding texte as an svg file named fichier_sortie"""
	output_file=open(fichier_sortie,'w')
	qr = QrCode.encode_text(texte, niveau_correction)
	#print_qr(qr)
	output_file.write(qr.to_svg_str(2))
	#en paramètre la largeur du cadre blanc autour du code en nb de carrés élémentaires
	output_file.close()


######################################
def genere_fichiers_svg(L_etu:List[Etudiant], dossier_temp:str, niveau_correction):
	""" Génère un fichier svg contenant son qr-code pour chaque élève de la liste L_etu.
	Le niveau de correction des erreurs des qr-codes est fixé par niveau_correction.
	Les fichiers générés sont stockés dans le dossier temporaire dossier_temp
	"""
	cpt=0
	for etu in L_etu:
		fichier_sortie=dossier_temp+"/code_etu_"+str(cpt)+".svg"
		genere_qr_code_svg(etu[3], fichier_sortie,niveau_correction)
		cpt=cpt+1



######################################
# GENERATION PDF 
######################################
def insere_svg(page:fitz.Page, rectangle:fitz.Rect, fichier_svg:str):
	""" Insère sur page le qr code contenu dans fichier_svg.
	La taille du qr-codes est la taille maximale tenant dans rectangle 
	"""
	qrcode=fitz.open(fichier_svg)
	qrcodebytes=qrcode.convertToPDF()
	qrcodepdf=fitz.open("pdf", qrcodebytes)
	page.showPDFpage(rectangle,qrcodepdf, 0)


######################################
def trace_etiquette(page:fitz.Page, x, y, nom:str, prenom:str, num:str, fichier_svg:str, f:myFormat):
	""" Trace sur page l'étiquette pour l'étudiant (nom, prenom, num, mail) où mail est encodé dans le qr-code donné par fichier_svg.
	La position de cette étiquette sur la page est donnée par (x,y) (coin en haut à gauche).
	Le format (taille et contenu) est fixé par les paramètres de f.
	"""
	shape = page.newShape()

	#le cadre global
	case = fitz.Rect(x, y, x+f.larg_etiq, y+f.haut_etiq)
	page.drawRect(case, color=(0.7,0.7,0.7))
	
	#le qr code
	case_code=fitz.Rect(x+f.larg_etiq/2+f.securite, y+f.securite, x+f.larg_etiq-f.securite, y+f.haut_etiq-f.securite)
	insere_svg(page,case_code, fichier_svg) 
	
	#les détails: ue, date, salle
	case_details=fitz.Rect(x+2*f.securite, y+f.securite, x+f.larg_etiq/2+f.securite, y+f.haut_etiq/2)
	page.drawLine((x+2*f.securite, y+2*f.securite),(x+2*f.securite,y+f.haut_etiq/2))
	#page.drawRect(case_details)
	shape.insertTextbox(case_details,"  "+f.ue+"\n  "+f.date+"\n  "+f.salle,fontsize=f.fontsize_details,fontname=f.police)
	#print("***case_details.height=",case_details.height,"et f.haut_etiq/2-f.securite=",f.haut_etiq/2-f.securite) 

	#le nom ou le numéro de l'etu
	if f.avec_noms:
		case_nom=fitz.Rect(x+2*f.securite, y+f.haut_etiq/2+f.securite, x+f.larg_etiq/2+f.securite, y+f.haut_etiq*3/4-f.securite)
		case_prenom=fitz.Rect(x+2*f.securite, y+f.haut_etiq*3/4, x+f.larg_etiq/2+f.securite, y+f.haut_etiq-2*f.securite)
		#page.drawRect(case_nom)
		#page.drawRect(case_prenom)
		
		#écrire le nom, taille ajustée si besoin
		fs=adapted_fontsize(f.fontsize_nom,f.police,nom,case_nom)
		shape.insertTextbox(case_nom,nom,fontsize=fs) 
		#écrire le prénom, taille ajustée si besoin
		fs=adapted_fontsize(f.fontsize_nom,f.police,prenom,case_prenom)
		shape.insertTextbox(case_prenom,prenom,fontsize=fs, fontname=f.police)

	else :
		case_num=fitz.Rect(x+2*f.securite,y+f.haut_etiq/2+f.securite,x+f.larg_etiq/2+f.securite, y+f.haut_etiq-f.securite)
		
		#page.drawRect(case_num)
		fs=adapted_fontsize(f.fontsize_num,f.police,str(num),case_num)
		shape.insertTextbox(case_num,str(num),fontsize=fs, fontname=f.police)

	shape.commit()  




######################################	
def genere_fichier_pdf(fichier_sortie_pdf:str, L_etu:List[Etudiant], f:myFormat):
	"""	Génère le fichier PDF des étiquettes de tous les étudiants de la liste L_etu.
	Ce fichier est nommé fichier_sortie_pdf, et son format est défini par les paramèteres de f.
	"""
	#creer un doc PDF
	doc = fitz.open()
	page=doc.new_page(-1, width=f.larg_feuille, height=f.haut_feuille)#-1 = à la fin 

	cpt = 0
	l=0
	c=0
	y = f.top_margin+f.securite
	x = f.left_margin +f.securite
	
	for etu in L_etu:
		#print(etu)
		try :
			#les donnes de l'etu
			nom,prenom,num,_=etu
		except Exception as e : 
		#on ne devrait pas rentrer dans ce genre d'exception pour une question de nombre de champs sur la ligne vu les précautions prises dans lit_csv
			print ("\n *** Pour la ligne ", etu,"\n *** on a l'exception <<",e,">>")
			print(" *** Merci de vérifier le fichier csv donné en entrée.")
			print(" *** Pour rappel chaque ligne doit être au format : nom, prénom, numéro, mail")
			print(" *** Il y a donc 4 champs par ligne, et seulement 3 virgules.")
			return -1
		
		fichier_svg="qrcodes_temp/code_etu_"+str(cpt)+".svg"
		
		trace_etiquette(page,x,y,nom,prenom,num, fichier_svg,f)
		
		#incrémentation				
		cpt=cpt+1
		#changement de colone
		c=c+1
		x=x+f.larg_etiq+2*f.securite
		#si on dépasse la dernière colonnne, revenir à la ligne
		if c== f.nbc: 
			c=0
			x = f.left_margin +f.securite
			l=l+1
			y=y+f.haut_etiq+2*f.securite
			if l==f.nbl:
				page=doc.new_page(-1, width=f.larg_feuille, height=f.haut_feuille)#-1 = à la fin 
				l=0
				y = f.top_margin+f.securite
				
	doc.save(fichier_sortie_pdf)




######################################
# AFFICHAGE INTERFACE
######################################
def affiche_bienvenue_fr():
	"""Affiche en français un joli titre encadré et bienvenue"""
	print("\n\n")
	#print("     /--------------------------------------------------------------------------\\")
	#print("     | Générateur d'étiquettes avec qr-codes pour l'envoi automatique de copies |")
	#print("     \--------------------------------------------------------------------------/")
	print("   /------------------------------------------------------------\\")
	print("   | Générateur d'étiquettes pour l'envoi automatique de copies |")
	print("   \------------------------------------------------------------/")
	print("\n======= Bienvenue!")
	#print("\nBienvenue!")
	#print("À tout moment vous pouvez quitter en tapant control+c ")


######################################
def affiche_conseils_debut_fr():
	""" Affiche en français des conseils aux utilisateurs novices, au début du processus"""
	print("\n\n\n------------ Pré-requis")
	print("-----------------------")
	print("......[avant]......\nPour générer les étiquettes il vous faut :")
	print(" - un fichier .csv* avec nom, prénom, numéro et mail des étudiants,")
	print(" - Python3 (on dirait que c'est bon :)")
	print(" - la librairie PyMuPDF")
	print(" - la librairie qrcodegen (ou le fichier qrcodegen.py dans le dossier courant)")
	print(" - des planches d'étiquettes autocollantes,")
	print(" - une imprimante qui accepte ces planches.")
	
	print("\n......[pendant]......\nIl faut que chaque personne qui compose :")
	print(" - colle son étiquette sur la PREMIÈRE PAGE de sa copie,")
	print(" - n'endommage pas le QR-code (ni rature, ni blanco...),")
	print(" - reporte son nom ou numéro sur CHAQUE FEUILLE de sa copie.")
	
	print("\n......[après]......\nPour distribuer automatiquement les copies, il faudra que chaque personne qui corrige :")
	print(" - puisse facilement scanner un paquet de feuilles recto-verso en un fichier PDF,") 
	print(" - ait une adresse @sorbonne-université.fr,")
	print(" - connaisse ses identifiants CAS (utilisés pour l'intranet de SU et Moodle Sciences par exemple),")
	print(" - soit capable de lancer un exécutable sur le PDF.\n")
	
	rep=input("* Si vous ne savez pas ce qu'est un fichier .csv tapez + , sinon tapez entrée.  ")
	if rep=="+":
		print("\n")
		affiche_expli_csv_fr()
		rep=input("\nPour continuer, tapez entrée.  ")
		
	print("\n\n\n------------ Possibilités")
	print("-------------------------")
	print("Ce programme permet de :")
	print(" - paramétrer les formats de vos étiquettes (en fonction des planches dont vous disposez),")
	print(" - choisir le niveau de correction des erreurs des qr-codes généres,")
	print(" - choisir si les étiquettes sont anonymes : au choix le nom et le prénom ou bien le numéro est indiqué (pas les deux),")
	print(" - stocker ces paramètres pour ne pas avoir à les saisir à nouveau la fois suivante,")
	print(" - choisir les détails apparaissant sur les étiquettes : UE, date et salle,")
	print(" - générer les étiquettes dans un fichier pdf au nom de votre choix.")
	print("\nPour l'envoi automatique des copies à partir de leur scan en PDF, utilisez [A REMPLIR]")
	rep=input("\nPour continuer, tapez entrée.  ")
	
	print("\n\n\n------------ Précisions")
	print("-----------------------")
	print("1) En principe il faut un fichier .csv pour chaque salle d'examen.")
	print("   Cela évite d'avoir à partagerune planche d'étiquettes entre différentes salles.")
	print("2) Pour faciliter la distribution des étiquettes lors de l'épreuve,")
	print("   il vaut mieux trier les étudiants dans le .csv dans l'ordre où ils seront placés.")
	print("   à défaut les trier comme sur la liste d'émargement.")
	print("3) Si vous dérogez au point 1), il est important de trier les étudiants par salles dans le .csv.")
	print("4) Si vous avez beaucoup de salles à gérer pour une même épreuve,") 
	print("   utilisez un script pour lancer ce programme automatiquement sur chaque fichier .csv.")
	print("   Une trame de script en bash est proposée dans \"script_multi_salles.sh\".") 
	print("5) Si vous n'avez pas les noms et prénoms des étudiants de manière séparée,")
	print("   mettez nom et prénom dans la première colonne et laissez la deuxième colonne VIDE.")
	print("6) La troisième colonne peut contenir, selon les besoins, le numéro d'étudiant ou bien le numéro d'anonymat.")
	print("7) Même si les feuilles sont agrafées, chacune doit porter le nom ou le numéro de l'étudiant.")
	print("   En effet les feuilles seront désagrafées pour être scannées.")
	print("   De même les copies doubles seront déchirées en feuilles simples.")
	print("   Le nom ou le numéro doit donc apparaître en page 1/4 et 3/4 de chaque copie double.")
	print("8) Chaque qr-code stocke une seule information : l'adresse mail à laquelle la copie doit être envoyée.")
	print("   On fait confiance aux personnes qui corrigent pour respecter l'anonymat en s'abstenant de scanner les qr-codes.")
	print("9) Si vos planches d'étiquettes ne sont pas au format A4 portrait,")
	print("   entrez une première fois vos paramètres dans l'interface pour générer un fichier .json,")
	print("   puis modifiez à la main les paramètres larg_feuille_cm, haut_feuille_cm, larg_feuille, haut_feuille dans ce fichier.")
	rep=input("\nPour continuer, tapez entrée.  ")


######################################
def affiche_expli_csv_fr():
	"""Affiche en français quleques explications sur ce qu'est un fichier csv et comment on l'obtient"""
	print("\n------------ Explications .csv")
	print("------------------------------")
	print("\n---Ça veut dire quoi \"csv\" ?")
	print("Un fichier .csv est un fichier texte qui encode un tableau.")
	print("L'extension \".csv\" est une abréviation pour \"Comma-Separated Values, ce qui signifie que les valeurs sont séparées par des virgules.\"")
	print("Chaque ligne du tableau correspond à une ligne du fichier csv.")
	print("Les différentes cases d'une même ligne sont séparées par des virgules.")
	print("\n---Comment générer mon fichier .csv ?")
	print(" - Choisissez votre tableur préféré pour ouvrir votre liste d'étudiants initiale.")
	print(" - Faites-y les manipulations nécessaires pour avoir sur chaque ligne le nom, le prénom, le numéro, et l'adresse mail d'un étudiant.")
	print(" - Supprimez si besoin les colonnes inutiles : seules les 4 premières colonnes doivent être remplies.")
	print(" - Selon le tableur, enregistrez au format .csv (avec \"enregistrer sous\"), ou exportez les données au format .csv.")
	print("\n---Un exemple ?")
	print("           --------------")
	print("           | A | a | 1  |")
	print("           --------------                         |A,a,1 ")
	print("Ce tableau | B | b | 2  |  est encodé en .csv par |B,b,2 ")
	print("           --------------                         |C,,36")
	print("           | C |   | 36 |")
	print("           --------------")
	

######################################	
def affiche_conseils_fin_fr(inputfile, outputfile, ue, date , salle, arg_parser):
	""" Affiche en français des conseils aux utilisateurs novices, en fin de processus
			Prend en argument 5 str des paramètres pour donner un exemple de commande.
			Prend en dernier argument un objet argparse.ArgumentParser pour afficher l'usage
	"""
	print("\n\n\n------------ Pour l'impression")
	print("------------------------------")
	print("Les PDF générés doivent être imprimés :")
	print("  - en 1 page par feuille,")
	print("  - en recto uniquement,")
	print("  - sur des planches d'étiquettes autocollantes.")
	print("Pensez donc à charger autant de planches d'étiquettes que vous avez de pages à imprimer.")
	print("\nLa première fois faites préalablement un test sur une feuille simple.")
	print("Si le PDF généré ne coïncide avec vos planches d'étiquettes,")
	print("ajustez les valeurs des paramètres généraux, relancez le processus et testez à nouveau.")
	input("\nPour continuer, tapez entrée.  ")
	
	print("\n\n\n------------ Lors de l'épreuve")
	print("------------------------------")
	print("Les étiquettes sont à coller IMPÉRATIVEMENT sur la PREMIÈRE PAGE de la copie.")
	print("En effet, dans la phase d'envoi automatique, les copies seront séparées les unes des autres au niveau des pages avec étiquette.")
	print("De plus, afin de ne pas risquer de mélanger les feuilles des différentes copies lors du scan,")
	print("il faut demander aux étudiants de noter leur nom ou leur numéro sur le recto de chaque feuille.")
	print("S'ils composent sur des copies doubles, il faut donc reporter le nom ou le numéro deux fois par copie : en page 1/4 et 3/4.")
	input("\nPour continuer, tapez entrée.  ")
	
	print("\n\n\n------------ Pour une prochaine fois")
	print("-----------------------------------")
	print("Si vous avez saisis des paramètres généaraux, ils ont été enregistrés dans le fichier \""+fichier_param_gen+"\".")
	#print("Les paramètres généraux que vous avez saisis ont été enregistrés dans le fichier", fichier_param_gen)
	print("Si vous souhaitez garder les mêmes valeurs pour ces paramètres la prochaine fois,")
	print("il suffira de préciser l'ue, la date, la salle, le fichier d'entrée (.csv) et celui de sortie (.pdf).")
	print("\nPour aller plus vite, vous pouvez passer ces 5 arguments directement en ligne de commande.")
	arg_parser.print_usage()
	#arg_parser.print_help()
	print("\nConcrètement, pour les paramètres que vous avez utilisés à l'instant, ça donnerait :")
	print("    ./qr_label_generator.py  -i",inputfile," -o",outputfile," -d", date,"-u",ue," -s",salle)
	print("avec des guillemets autour des arguments contenant des espaces,")
	print("par exemple : -s \"amphi 12\", et non -s amphi 12.")
	print("\nChacun des paramètres est optionnel : si vous ne le précisez pas, sa valeur vous sera demandée par l'interface.")
	print("Si vous les précisez tous en revanche, vous pouvez désactiver l'interface grâce à l'option -q")
	print("(q comme \"quiet\" car cela rend le programme \"muet\"). Par exemple :")
	print("    ./qr_label_generator.py  -i",inputfile," -o",outputfile," -d", date,"-u",ue," -s",salle," -q")
	input("\n Pour continuer, tapez entrée.  ")


######################################
def affiche_au_revoir_fr():
	"""Affiche en français un joli au revoir encadré """
	print("\n\n")
	print("   /------------------------------------------------------------\\")
	print("   |                   Au revoir et à bientôt !                 |")
	print("   \------------------------------------------------------------/")
	print("\n\n")



######################################
# MAIN
######################################

def main():
	#objet pour parser les arguments (tous optionnels)
	parser = argparse.ArgumentParser(description="Génère PDF d'étiquettes à partir d'une liste d'étudiants au format .csv")
	#ajout des options attendues
	parser.add_argument('-i',metavar='inputfile',dest='inputfile', type=str,\
	help='précise que le fichier .csv contenant le liste des étudiants à prendre en entrée est nommé inputfile')
	parser.add_argument('-o',metavar='outputfile',dest='outputfile', type=str,\
	help='précise que le fichier .PDF contenant les étiquettes produites sera nommé ouputfile')
	parser.add_argument('-u',metavar='ue', dest='ue',type=str, help="précise le nom de l'ue à afficher sur les étiquettes")
	parser.add_argument('-d',metavar='date', dest='date',type=str, help="précise la date à afficher sur les étiquettes")
	parser.add_argument('-s',metavar='salle', dest='salle', type=str, help="précise  le nom de la salle à afficher sur les étiquettes")
	parser.add_argument('-q',dest='quiet_mode',action='store_true', help="rend le programme muet (\"quiet\") = inactive l'interface utilisateur \n cette option rend donc les arguments -i,-o, -u, -d et -s obligatoires, ainsi que la présence d'un fichier de paramètres valides nommé "+fichier_param_gen)
	#parser les arguments
	args = parser.parse_args()
	#petit affichage de test
	#print("ici le main")
	#print("args.inputfile =",args.inputfile)
	#print("args.outputfile =",args.outputfile)
	#print("args.ue =",args.ue)
	#print("args.date=",args.date)
	#print("args.salle =",args.salle)
	#print("args.quiet_mode =",args.quiet_mode)
	
	#QUIET MODE 
	if args.quiet_mode:
	
		#ce mode nécéssite que tous les paramètres et options soient bien définis
		manque_arg=args.inputfile==None or args.outputfile==None or args.ue==None or  args.date==None or args.salle==None
		manque_fichier=not os.path.isfile(fichier_param_gen)
		if manque_arg or manque_fichier:
			print("ERREUR : L'option -q rend le programme muet (\"quiet\").")
			print("         Autrement dit l'interface utilisateur est déscativée.")
			if manque_arg:
				print("         Cette option rend donc obligatoires les arguments -i,-o, -u, -d et -s .")
			if manque_fichier:
				print("         Cette option rend donc obligatoire la présence d'un fichier de paramètres valide nommé \""+fichier_param_gen+"\".")
				print("         Vous pouvez ajouter à la main un tel fichier, ou bien relancer la commande prcécédente sans l'option -q pour le créer grâce à l'interface.")
			return -1
		
		#si tout est a priori fourni
		#paramètre généraux
		try :
			mon_format=lit_param_gen_json(fichier_param_gen)
		except Exception as e :
			print("\n", e)
			return -1
			
		#parametre particuliers
		mon_format.ue=args.ue
		mon_format.date=args.date
		mon_format.salle=args.salle
		
		#lecture du csv
		L_etu=lit_csv(args.inputfile,False)

		#ecriture des svg
		try:
			os.mkdir(dossier_temp)
		except FileExistsError:
			shutil.rmtree(dossier_temp)
			os.mkdir(dossier_temp)
		genere_fichiers_svg(L_etu, dossier_temp,mon_format.niveau_correction)
		
		#ecriture du PDF
		genere_fichier_pdf(args.outputfile,L_etu,mon_format)
		
		#suppression des fichiers svg temporaires
		shutil.rmtree(dossier_temp)
		
	#INTERACTIVE MODE 
	else:
		
		#bienvenue et explications
		affiche_bienvenue_fr()
		rep=input("Pour commencer, souhaitez-vous quelques explications (à lire la première fois) ? (o/n)  ")
		if rep=="o":
			affiche_conseils_debut_fr()
		print("\n\n\n======= C'est parti!")
		
		#paramètre généraux
		try :
			mon_format=lit_param_gen_json(fichier_param_gen)
			print("\n\n\n------------  Paramètres généraux")
			print("---------------------------------")
			print("Nous avons détécté un fichier de paramétrage(",fichier_param_gen,").")
			print("Pour rappel, les paramètres sont les suivants:")
			mon_format.resume_param_gen_fr()
			mon_format.affiche_format_fr()
			rep=input("Continuer avec ces parametres (o/n)?  ")
			if rep=="n":
				raise Exception("---Vous avez rejeté les paramètres enregistrés")
		except Exception as e :
			print("\n", e)
			mon_format=rentrer_param_gen()

		#fichier entrée
		fichier_entree_csv=rentrer_fichier_entree_fr(args)

		#parametres particuliers
		rentrer_param_part_fr(mon_format,fichier_entree_csv,args)
		
		#fichier sortie
		fichier_sortie_pdf=rentrer_fichier_sortie_fr(args)

		#lecture du csv
		L_etu=lit_csv(fichier_entree_csv,True)

		#ecriture des svg
		try:
			os.mkdir(dossier_temp)
		except FileExistsError:
			print("\n\n\n------------  Génération des qr-codes au format .svg")
			print("----------------------------------------------------")
			print("Le dossier",dossier_temp, "existe déjà.")
			print("Il s'agit sûrement d'un dossier temporaire crée lors d'une précédente exécution interrompue trop tôt.")
			print("Si vous ne voulez pas écraser ce dossier, tapez control+C, renommez le dossier différement et recommencez.")
			rep=input("POur continuer (et donc supprimer ce dossier temporaire), tapez seulement entrée.  ")
			if rep=="":
				shutil.rmtree(dossier_temp)
			os.mkdir(dossier_temp)
		genere_fichiers_svg(L_etu, dossier_temp,mon_format.niveau_correction)
		
		#ecriture du PDF
		genere_fichier_pdf(fichier_sortie_pdf,L_etu,mon_format)
		shutil.rmtree(dossier_temp)
		
		#au revoir
		print("\n\n\n======= C'est fini!")
		print("Les étiquettes ont été générées dans le fichier",fichier_sortie_pdf)
		rep=input("\n\n\nPour finir, souhaitez-vous quelques conseils (à lire la première fois) ? (o/n)  ")
		if rep=="o" or rep=="O":
			affiche_conseils_fin_fr(fichier_entree_csv,fichier_sortie_pdf,mon_format.ue, mon_format.date, mon_format.salle,parser)
		affiche_au_revoir_fr()



######################################
# pour que le main s'execute
main()







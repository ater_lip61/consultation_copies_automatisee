Ce projet permet d'envoyer par mail à chaque étudiant sa copie scanée,
et ce de manière automatisée grâce à un système d'étiquettes avec QR code à coller sur les copies.
Ce projet est codé en python3. Le détail des librairies python requises est donné 
dans le fichier [`requirements.txt`](documentation/requirements.txt).

Le dossier [`documentation`](documentation) contient la documentation sous plusieurs formats 
([`markdown`](documentation/doc.md),
[`html`](documentation/doc.html) et
[`pdf`](documentation/doc.pdf))
ainsi que les 
ainsi que le script et le template utilisés pour générer les fichiers `.pdf` et `.html` à partir du fichier `.md`.

Le dossier [`sources`](sources) contient les trois scripts python à utiliser pour 
- générer des étiquettes ([`qr_generator.py`](sources/qr_generator.py));
- découper le PDF des copies scanées ([`splitter.py`](sources/splitter.py)) ;
- puis envoyer les copies individuellement ([`mailer.py`](sources/mailer.py)). 

Le dossier [`exemples`](exemples) montre comment utiliser ces scripts, ainsi que des fichiers factices pour les tester.
Le point d'entrée pour ces exemples est le fichier [`exemples_de_commandes.txt`](exemples/exemples_de_commandes.txt).

